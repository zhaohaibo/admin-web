package com.webside.base.basedao;

import java.util.Date;

import org.apache.commons.lang3.time.FastDateFormat;


public class MyBatisSql {
	private String sql;// 运行期的sql，带?
	private Object[] parameters;// ;//运行期的参数，与?相匹配
	private Class resultClass;// <select id="XXX" resultType="ZZZ">中的resultType

	public Class getResultClass() {
		return resultClass;
	}

	public void setResultClass(Class resultClass) {
		this.resultClass = resultClass;
	}

	public void setSql(String sql) {
		this.sql = sql;
	}

	public String getSql() {
		return sql;
	}

	public void setParameters(Object[] parameters) {
		this.parameters = parameters;
	}

	public Object[] getParameters() {
		return parameters;
	}
	
	@Override
	public String toString() {
		if (parameters == null || sql == null) {
			return "";
		}
		int i = 0;
		while (sql.indexOf("?") != -1 && parameters.length > 0) {
			if (parameters[i] == null) {
				sql = sql.replaceFirst("\\?", "null");
			}
			else {
				String str = parameters[i].toString();
				if (parameters[i] instanceof Date) {
					str = FastDateFormat.getInstance("yyyy-MM-dd HH:mm:ss").format(parameters[i]);
				}
				sql = sql.replaceFirst("\\?", parameters[i] == null? "null" : "'" + str  + "'");
			}
			i++;
		}
		return sql.replaceAll("(\r?\n(\\s*\r?\n)+)", "\r\n");
	}
}