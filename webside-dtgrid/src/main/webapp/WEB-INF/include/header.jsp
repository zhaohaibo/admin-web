<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%--<%@ taglib uri="/WEB-INF/tlds/i18n.tld" prefix="i18n"%>--%>
<c:set var="ctx" value="${pageContext.request.contextPath}" />

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="zh-cn" xmlns="http://www.w3.org/1999/xhtml">
<head>
    <link rel="stylesheet" href="${ctx}/resources/js/bootstrap/bootstrap.min.css"/>
    <link rel="stylesheet" href="${ctx}/resources/fonts/fontawesome/font-awesome.min.css" media="all"/>
    <link rel="stylesheet" href="${ctx}/resources/fonts/opensans/ace-fonts.min.css"/>
    <link rel="stylesheet" href="${ctx}/resources/css/ace/ace.min.css" class="ace-main-stylesheet" id="main-ace-style"/>
    <link rel="stylesheet" type="text/css" href="${ctx}/resources/js/dlshouwen.grid.v1.2.1/dlshouwen.grid.min.css" />
    <!--[if lte IE 9]>
    <link rel="stylesheet" href="${ctx}/resources/css/ace/ace-part2.min.css" />
    <![endif]-->
    <!--[if lte IE 9]>
    <link rel="stylesheet" href="${ctx}/resources/css/ace/ace-ie.min.css" />

    <link rel="stylesheet" href="${ctx}/resources/css/customer/tabs-menu.css">

    <![endif]-->
    <style>
        body{background-color: white;overflow-x: hidden;}
    </style>
    <!-- JQuery script -->
    <!-- 非IE浏览器不会识别IE的条件注释，所以这里判断非IE需要如下写法：参照下面jquery-2.1.4.min.js引入的方式 -->
    <!--[if !IE]><!-->
    <script type="text/javascript" src="${ctx}/resources/js/jquery/jquery-2.1.4.min.js"></script>
    <!--<![endif]-->
    <!--[if IE]>
    <script type="text/javascript" src="${ctx}/resources/js/jquery/jquery-1.11.3.min.js"></script>
    <![endif]-->
    <!-- basic scripts -->
    <script type="text/javascript">
        if ('ontouchstart' in document.documentElement)document.write("<script src='${ctx}/resources/js/jquery/jquery.mobile.custom.min.js'>" + "<"+"script>");
    </script>
    <!--[if lt IE 9]>
    <script type="text/javascript" src="${ctx}/resources/js/ie/html5shiv.js"></script>
    <script type="text/javascript" src="${ctx}/resources/js/ie/respond.min.js"></script>
    <![endif]-->
    <!--[if lt IE 8]>
    <script src="${ctx}/resources/js/ie/json2.js"></script>
    <![endif]-->
    <!--[if lte IE 8]>
    <script type="text/javascript" src="${ctx}/resources/js/ie/excanvas.min.js"></script>
    <![endif]-->
    <script type="text/javascript" src="${ctx}/resources/js/bootstrap/bootstrap.min.js"></script>
    <script src="${ctx}/resources/js/jqueryui/jquery-ui.min.js" type="text/javascript"></script>
    <script src="${ctx}/resources/js/jqueryui/jquery.ui.touch-punch.min.js" type="text/javascript"></script>
    <script type="text/javascript" src="${ctx}/resources/js/layer-v2.3/layer.js"></script>
    <script type="text/javascript" src="${ctx}/resources/js/dlshouwen.grid.v1.2.1/dlshouwen.grid.min.js"></script>
    <script type="text/javascript" src="${ctx}/resources/js/dlshouwen.grid.v1.2.1/i18n/zh-cn.js"></script>
    <script type="text/javascript" src="${ctx}/resources/js/nicescroll/jquery.nicescroll.min.js"></script>
    <!-- 元素动画 -->
    <script type="text/javascript" src="${ctx}/resources/js/scrollreveal/scrollreveal.min.js"></script>

    <!--自定义JS文件-->
    <script type="text/javascript" src="${ctx}/resources/js/customer/index/index.js"></script>
    <script type="text/javascript" src="${ctx}/resources/js/customer/common/common.js"></script>
    <script type="text/javascript" src="${ctx}/resources/js/customer/common/tabs-menu.js"></script>

    <script type="text/javascript" type="text/javascript">
        var sys = sys || {};
        sys.rootPath = "${ctx}";
        sys.pageNum = 10;
        sys.gridStyle = "Bootstrap";
        window.scrollreveal = ScrollReveal();

        function addTab(menu_id, menu_title, menu_url) {
            openTab(sys.rootPath + menu_url, menu_title,  false);
        }


        $(function(){
            $(document).ajaxSend( function(event, jqXHR, options){
                var loc = "/index.html";
                if(options.data == undefined) {
                    if(options.url.indexOf("?") != -1) {
                        options.url += "&baseUri=" + encodeURIComponent(loc);
                    } else {
                        options.url += "?baseUri=" + encodeURIComponent(loc);
                    }
                } else {
                    options.url += "?baseUri=" + encodeURIComponent(loc);
                    options.data = "baseUri=" + encodeURI(loc) + "&" +options.data;
                }
            });
        });

    </script>
</head>